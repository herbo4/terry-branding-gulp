var gulp = require('gulp');
var less = require('gulp-less');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');

// Compiles SCSS files from /scss into /css
gulp.task('sass', function () {
 return gulp.src('scss/app.scss')
  .pipe(sass())
  .pipe(gulp.dest('css'))
  .pipe(browserSync.reload({
   stream: true
  }))
});

// Minify compiled CSS
gulp.task('minify-css', ['sass'], function () {
 return gulp.src('css/app.css')
  .pipe(cleanCSS({ compatibility: 'ie8' }))
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('css'))
  .pipe(browserSync.reload({
   stream: true
  }))
});

// Minify JS
gulp.task('minify-js', function () {
 return gulp.src('js/app.js')
  .pipe(uglify())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('js'))
  .pipe(browserSync.reload({
   stream: true
  }))
});

// Run everything
gulp.task('default', ['sass', 'minify-css', 'minify-js', 'copy']);

// Configure the browserSync task
gulp.task('browserSync', function () {
 browserSync.init({
  server: {
   baseDir: ''
  },
 })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'sass', 'minify-css', 'minify-js'], function () {
 gulp.watch('scss/*.scss', ['sass']);
 gulp.watch('css/*.css', ['minify-css']);
 gulp.watch('js/*.js', ['minify-js']);
 // Reloads the browser whenever HTML or JS files change
 gulp.watch('*.html', browserSync.reload);
 gulp.watch('js/**/*.js', browserSync.reload);
});
